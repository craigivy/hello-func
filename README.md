# hello-func
A simple hello world HTTP cloud function deployed with terraform

## Setup
* Install terrafrom and gcloud
* Login to Google Cloud Platform(GCP) using gcloud
```
gcloud auth login {user}
```
* Create a new GCP project
* Setup the gitlab project on your environment
```
git clone https://gitlab.com/craigivy/hello-func.git
cd hello-func/terraform
```

## Installing project using terraform
```
terraform init
terraform apply -auto-approve -var="project=YOUR_PROJECT"
```
## Using the application
```
export APIG=$(terraform output -raw api_hostname)

# get
curl https://${APIG}/name

# get 100
. get.sh 100
```