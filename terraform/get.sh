#!/bin/bash

export APIG=$(terraform output -raw api_hostname)
for i in $(eval echo {1..$1});
do
  ts=$(date +%s%N)  
  set -x
  curl -X GET https://${APIG}/name
  { set +x; } 2>/dev/null
  printf "\n"
  echo "[took $((($(date +%s%N) - $ts)/1000000)) millis]"
  printf "\n"
done