#!/bin/bash  

export FUNC_URL=$(terraform output -raw func_url)
for i in $(eval echo {1..$1});
do
  ts=$(date +%s%N)  
#  set -x
  echo "${FUNC_URL} "
  curl -H "Authorization: Bearer $(gcloud auth print-identity-token)" -X GET ${FUNC_URL}
#  { set +x; } 2>/dev/null
  printf "\n"
  echo "[took $((($(date +%s%N) - $ts)/1000000)) millis]"
  printf "\n"

done