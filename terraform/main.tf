terraform {

  #   backend "gcs" {
  #   bucket  = "tf-state-prod"
  #   prefix  = "terraform/state"
  # }

  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.5.0"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = "~> 3.0"
    }

  }
}

provider "google" {

  //  credentials = file("<NAME>.json")

  project = var.project
  region  = var.region
  zone    = "us-central1-c"
}

provider "google-beta" {

  //  credentials = file("<NAME>.json")

  project = var.project
  region  = var.region
}

resource "google_project_service" "cloudbuild-service" {
  service                    = "cloudbuild.googleapis.com"
  # disable_dependent_services = true
}

resource "google_project_service" "cloudfunctions-service" {
  service                    = "cloudfunctions.googleapis.com"
  # disable_dependent_services = true
}

resource "google_storage_bucket" "func_bucket" {
  name          = "${var.region}_${var.project}_source_func_bucket"
  force_destroy = true
}

module "func_get" {
  source = "./modules/functions"

  func_key = "get"
  name = "get"
  region = var.region
  project = var.project

  depends_on = [google_storage_bucket.func_bucket,google_project_service.cloudbuild-service,google_project_service.cloudfunctions-service]
}

resource "google_project_service" "apigateway" {
  service                    = "apigateway.googleapis.com"
  # disable_dependent_services = true
}

resource "google_project_service" "servicemanagement-service" {
  service                    = "servicemanagement.googleapis.com"
  depends_on = [google_project_service.apigateway]
  disable_dependent_services = true
}

resource "google_project_service" "servicecontrol-service" {
  service                    = "servicecontrol.googleapis.com"
  depends_on = [google_project_service.servicemanagement-service]
  # disable_dependent_services = true
}

resource "google_api_gateway_api" "name_api" {
  provider = google-beta
  api_id   = "name-api"
  depends_on = [google_project_service.servicecontrol-service]
}

resource "google_api_gateway_api_config" "name_cfg" {
  provider      = google-beta
  api           = google_api_gateway_api.name_api.api_id
  api_config_id = "name-cfg"

  openapi_documents {
    document {
      path     = "spec.yaml"
      contents = base64encode(templatefile("${path.module}/openapi2.yaml.tpl", { REGION=var.region, PROJECT=var.project}))
    }
  }

  gateway_config {
    backend_config {
     google_service_account = "${var.project}@appspot.gserviceaccount.com"
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "google_api_gateway_gateway" "name_gw" {
  provider   = google-beta
  api_config = google_api_gateway_api_config.name_cfg.id
  gateway_id = "api-gw"
}


