variable "func_key" {
  type        = string
  description = "The func_key.'"
}

variable "name" {
  type        = string
  description = "Name of the function which will be used in the URL."
}

variable "project" {
  type        = string
  description = "The default project.'"
}

variable "region" {
  type        = string
  description = "The default region.'"
  default = "us-central1"
}