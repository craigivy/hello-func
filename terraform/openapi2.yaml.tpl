swagger: "2.0"
info:
  title: Hello World
  description: "Hello World"
  version: "1.0.0"
schemes:
  - "https"
paths:
  "/name":
    get:
      description: "Hello World"
      operationId: "helloworld"
      x-google-backend:
        address: https://${REGION}-${PROJECT}.cloudfunctions.net/get
      responses:
        200:
          description: "Success."
          schema:
            type: string
