output "api_hostname" {
    value = google_api_gateway_gateway.name_gw.default_hostname
}
output "func_url" {
    value = module.func_get.https_url
}