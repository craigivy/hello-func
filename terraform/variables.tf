variable "project" {
  type        = string
  description = "The default project.'"
}

variable "region" {
  type        = string
  description = "The default region.'"
  default = "us-central1"
}

variable "location_id" {
  type        = string
  description = "The default App Engine region.'"
  default     = "us-central"
}
